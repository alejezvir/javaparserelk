import java.io.Serializable;

public class NewsTemplate implements Serializable {
    private String date;
    private String title;
    private String paragraph;

    NewsTemplate(){

    }

     NewsTemplate setDate(String date) {
        this.date = date;
        return this;
    }

     NewsTemplate setTitle(String title) {
        this.title = title;
        return this;
    }

     NewsTemplate setParagraph(String paragraph) {
        this.paragraph = paragraph;
        return this;
    }

    @Override
    public String toString() {
        return String.format("%s\n%s\n%s\n%s", date, title, paragraph,"-".repeat(100));
    }
}


