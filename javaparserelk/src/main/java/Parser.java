
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class Parser implements Runnable{
    private Document doc = null;

    Parser(){}


    @Override
    public void run() {
        String s = "";
        if (Connect.listHref.size() != 0) {
            while (Connect.listHref.size() != 0) {
                synchronized (Connect.listHref) {
                    s = Connect.listHref.get(0);
                    Connect.listHref.remove(0);
                }

                NewsTemplate page = ConnectToUrlOfPage(s).DataFromPage();
                synchronized (Connect.listOfPage) {
                    Connect.listOfPage.add(page);
                }

            }
        }
        System.out.printf("%s: ",Thread.currentThread().getName());
        Thread.currentThread().interrupt();

    }

    private NewsTemplate DataFromPage(){
        Elements paragraphs = doc.select("div");
        Elements titles = doc.select("h1");

        NewsTemplate newsTemplate = new NewsTemplate();


        for (Element title: titles) {
            if (title.attr("class").equals("h1-title")) {
                newsTemplate
                        .setTitle(title.text());
                break;
            }
        }

        for(Element paragraph: paragraphs) {
            if (paragraph.attr("class").equals("insides-page__news__text  insides-page__news__text_with-popular")) {
                newsTemplate
                        .setParagraph(paragraph.text());
                break;
            }
        }

        for(Element date: paragraphs) {
            if (date.attr("class").equals("news__date")) {
                newsTemplate
                        .setDate(date.text());
                break;
            }
        }
        return newsTemplate;
    }
    /* Метод для подключения к url из массива,
    *  полученного из LinksfromMainPage
    *
    * */
    private Parser ConnectToUrlOfPage(String url) {
        System.out.printf("Получение данных из URL: %s... ", url);
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            System.out.println("Ошибка при подключении к URL");
            e.getStackTrace();
        }
        System.out.println("Успешно");
        return this;
    }
}

