import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Connect {
    private String url;
    private Document doc = null;
    public  static volatile ArrayList<String> listHref = new ArrayList<>();
    public  static List<NewsTemplate> listOfPage = new ArrayList<>();

    public Connect(String s){
        ConnectToURL(s).LinksFromMainPage();
    }
    private Connect ConnectToURL(String url) {
        this.url = url;
        System.out.printf("Получение данных из URL: %s...\n", url);
        try {
            doc = Jsoup.connect(this.url).get();

        } catch (IOException e) {
            System.out.println("Ошибка при подключении к URL");
            e.getStackTrace();
        }
        System.out.println("Успешно");
        return this;
    }
// hello everyone
    private void LinksFromMainPage() {
        Elements Newsblocks = doc.select("div");

        for (Element Newsblock: Newsblocks) {
            if (Newsblock.attr("class").equals("news  ")){
                Elements links = Newsblock.select("a[href]");
                for (Element link: links) {
                    if (link.attr("abs:href").contains("episode")) {
                        Connect.listHref.add(link.attr("abs:href"));
                    }
                }

            }
        }

    }
}
