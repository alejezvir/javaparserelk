import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
//import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.rest.RestStatus;
import org.jsoup.helper.Validate;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) throws IOException {
        Validate.isTrue(args.length == 1, "usage: supply url to fetch");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        List<String> listOfPageInJSON = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String in = "";


        while (!in.equals("Exit")) {
            System.out.printf("%s", "Введите команду: \n");
            in = reader.readLine();
            switch (in) {
                case "connect":
                    long startTime = System.currentTimeMillis();
                    Connect connect = new Connect(args[0]);


/*Connect.listHref.stream().parallel().forEach(
        link -> {
            Connect.listHref.remove(0);
            System.out.println(parser.ConnectToUrlOfPage(link).DataFromPage());
        }
);*/
                for (int i = 0; i < 100; i++) {
                    new Thread(new Parser()).start();
                }

                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                    for (NewsTemplate item : Connect.listOfPage
                ) {
                    try {

                        listOfPageInJSON.add(objectMapper.writeValueAsString(item));
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }


                for (String s : listOfPageInJSON
                ) {
                    System.out.println(s);
                }
                System.out.printf("%s\n%s %s\n", "-".repeat(100), "Количество статей:", listOfPageInJSON.size());
                long time = System.currentTimeMillis() - startTime;
                System.out.println("Время выполнения: " + time);
                break;

                case "el":
                    RestHighLevelClient client  =
                    new RestHighLevelClient(ElasticRestConnector.getInstance());

                    IndexResponse indexResponse = null;
                    try {

                        indexResponse = client.index(ElasticRestConnector.indexRequestCreate(listOfPageInJSON.get(0), "doc" , "parsedtitles", "1"));

                    } catch (ElasticsearchException e) {
                        if (e.status() == RestStatus.CONFLICT) {
                            System.out.printf("%s", "Кофликт версий REST клиента и ElasticSearch\n");
                        }
                    }
                    int a = 0;
                    ElasticRestConnector.responseIndex(indexResponse);

                    GetResponse getResponse = null;
                    try {
                         getResponse = client.get(ElasticRestConnector.getRequest("parsedtitles", "doc",  "1"));
                    } catch (ElasticsearchException e) {
                        if (e.status() == RestStatus.NOT_FOUND) {
                            System.out.printf("%s", "Нет такого документа\n");
                        }
                    }

                    ElasticRestConnector.responseGet(getResponse);
                    ElasticRestConnector.closeConnection();
                 break;
            }
        }
        reader.close();
    }
}