import org.apache.http.HttpHost;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.replication.ReplicationResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;


class ElasticRestConnector {
    private static RestClient restClient = RestClient.builder(
            new HttpHost("10.0.1.139", 9200, "http"),
            new HttpHost("10.0.1.139", 9300, "http")).build();

    private ElasticRestConnector(){}


    static RestClient getInstance() {
        return restClient;
    }

    static void closeConnection(){
        try {
            restClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    static IndexRequest indexRequestCreate(String s, String type, String title, String id) {

        return  new IndexRequest(title, type, id)
                .timeout(TimeValue.timeValueSeconds(0))
                .opType("create").source(s, XContentType.JSON);
    }

    static IndexRequest indexRequestUpdate(String s, String title, String id) {

        return new IndexRequest(title)
                .id(id).source(s, XContentType.JSON).opType("update");
    }

    static GetRequest getRequest( String title, String type, String id){

        return new GetRequest(title, type, id);
    }


    static void responseIndex(IndexResponse indexResponse){

        String index = indexResponse.getIndex();
        String id = indexResponse.getId();
        if (indexResponse.getResult() == DocWriteResponse.Result.CREATED) {
            System.out.printf("%s %s %s. %s %s\n", "Документ с индексом", index, "создан", "ID документа", id);

        } else if (indexResponse.getResult() == DocWriteResponse.Result.UPDATED) {
            System.out.printf("%s %s %s. %s %s\n", "Документ с индексом", index, "изменен", "ID документа", id);
        }
        ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();
        if (shardInfo.getTotal() != shardInfo.getSuccessful()) {

        }
        if (shardInfo.getFailed() > 0) {
            for (ReplicationResponse.ShardInfo.Failure failure :
                    shardInfo.getFailures()) {
                String reason = failure.reason();
                System.out.printf("%s", reason);
            }
        }
    }

    static String responseGet(GetResponse getResponse){
        if (getResponse.equals(null)){
            System.out.printf("%s", "Нулевая ссылка вместо ответа\n");
            return "";
        }
        String index = getResponse.getIndex();
        String id = getResponse.getId();
        String sourceAsString = "";
        if (getResponse.isExists())
            sourceAsString =  getResponse.getSourceAsString();
        return String.format("%s %s %s\n","Индекс" + index, "ID" + id, sourceAsString);
    }
}
